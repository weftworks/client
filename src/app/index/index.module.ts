import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module';
import { FormModule } from '../form/form.module';
import { IndexComponent } from './index.component';

@NgModule({
  imports: [
    CoreModule,
    FormModule
  ],
  declarations: [
    IndexComponent
  ]
})
export class IndexModule { }
